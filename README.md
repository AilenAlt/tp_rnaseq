# Análisis de datos de RNA-seq
## Identificación de genes diferencialmente expresados en girasol

El objetivo principal de este trabajo consiste en identificar los genes de girasol que presentan una expresión diferencial al comparar dos condiciones experimentales distintas: el tiempo 0 hs post inoculación y el tiempo 8 hs post inoculación.
## Contenido
- **Archivos _htseqcount_**
	- Tiempo 0 hs post inoculación:
		- *C1VG2ACXX_1_21_RK416_0_I.trim.tally.fastq.htseq.count*
		- *C1VG2ACXX_2_9_RK416_0_I.trim.tally.fastq.htseq.count*
	- Tiempo 8 hs post inoculación:
		- *C1VG2ACXX_2_11_RK416_8_I.trim.tally.fastq.htseq.count*
		- *C1VG2ACXX_1_7_RK416_8_I.trim.tally.fastq.htseq.count*
- **Descripción del diseño experimental**: *exp_design.tsv*
- **Análisis de expresión diferencial utilizando _edgeR_**: *RNA_seq_all.r*
- **Resultados, lista de genes diferencialmente expresados:** *de_fdr05.txt*
