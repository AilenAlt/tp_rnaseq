setwd("/home/INTA_rna_seq/informe")
library(edgeR)
library(graphics)

#Cargo el diseño experimental
expDesing = readTargets("exp_design.tsv")

#exploro el dataframe
class(expDesing)
head(expDesing)

#Creo el objeto de clase DGElist, usando el archivo con informacion
#sobre el diseño experimental lee los archivos con los datos de 
#cada muestra
raw_counts = readDGE(expDesing, header = FALSE)

#Exploro el objeto raw_counts
head(raw_counts$counts)
class(raw_counts$counts)
dim(raw_counts$counts)
class(raw_counts)
names(raw_counts)
rownames(raw_counts)
colnames(raw_counts)
summary(raw_counts$samples$lib.size)

#Exploro los datos
#Grafico en el que se respresentan las diferencias de las raw counts entre
#muestras en dos dimensiones
plotMDS(raw_counts$counts, top = 1000, labels = raw_counts$samples$line)

#Filtro segun el valor de CPM
cpm_filter = rowSums(cpm(raw_counts$counts)>1)>=2

#exploro el objeto cpm_filter
tail(cpm_filter)
head(cpm_filter)
table(cpm_filter)
  # FALSE  TRUE 
  # 46809 49552 

#Uso la tabla con booleanos para filtrar el objeto raw_counts
dge_cpm_filter = raw_counts[cpm_filter,]

#dge_cpm_filter es el objeto que contiene las
#raw counts filtrado por cpm

dim(dge_cpm_filter)
head(dge_cpm_filter$counts)
class(dge_cpm_filter)

#Comparo las raw counts antes y despues de filtrar por CPM
boxplot(raw_counts$counts, ylim = c(0, 1000), main="Raw counts")
boxplot(dge_cpm_filter$counts, ylim = c(0, 1000), main="Filtered counts")

#Normalizacion
dge_cpm_filter <- calcNormFactors(dge_cpm_filter)
dge_cpm_filter$samples$norm.factors

#Factores de normalizaciòn cercanos a 1, lo esperado

##Evaluaciòn de la expresion diferencial de genes

#Estimaciòn de la dispersiòn: Common, Trended y Tagwise
dge_cpm_filter = estimateCommonDisp(dge_cpm_filter)
dge_cpm_filter = estimateTrendedDisp(dge_cpm_filter)
dge_cpm_filter = estimateTagwiseDisp(dge_cpm_filter)

#Grafico BCV
names(dge_cpm_filter)
plotBCV(dge_cpm_filter)

#Exploro los valores de tagwise dispersion
summary(dge_cpm_filter$tagwise.dispersion)
boxplot(dge_cpm_filter$tagwise.dispersion)
#En el boxplot se observa que la mayoria de los
#valores de tagwise dispersion son cercanos a 0

#Comparaciòn con conteos sin filtrar ni normalizar
rawDisp <- estimateCommonDisp(raw_counts)
rawDisp <- estimateTrendedDisp(rawDisp)
rawDisp <- estimateTagwiseDisp(rawDisp)
boxplot(rawDisp$tagwise.dispersion)
plotBCV(rawDisp)
#El gràfico de trended BCV no sigue un patron en particular, el BCV
#varìa mucho a lo largo del eje de las CPM

##Test de expresion diferencial
de = exactTest(dge_cpm_filter, dispersion = "tagwise", rejection.region = "doubletail")

#Exploro el objeto "de"
names(de)
class(de)
head(de$table)
class(de$table)
head(de$comparison)
dim(de$table)

#P-valor corregido (FDR)
de_fdr = topTags(de, n = nrow(de))

#Exploro el objeto de_fdr
names(de_fdr)
class(de_fdr)
dim(de_fdr$table)
head(de_fdr$table)

#Conservo genes con FDR menor a 0.05
de_fdr05 = subset(de_fdr$table, FDR < 0.05)
dim(de_fdr05)
head(de_fdr05)
tail(de_fdr05)

#Guardo la tabla con los genes con expresiòn
#diferencial significativa
write.table(de_fdr05, "de_fdr05.txt", sep = "\t")

#La distribucion de P values es homogenea para valores mayores 
#a 0.05
hist(de$table$PValue, breaks = 20)

#Grafico para ver el FC vs CPM de todos los genes, coloreando
#los diferencialmente expresados
plotSmear(de, main = "plotSmear")
rownames(de_fdr05)
plotSmear(de, de.tags=rownames(de_fdr05), main = "plotSmear")

#En rojo los genes diferencialmente expresados


